import math
import random
import numpy as np
from envs.omni_env import OmniEnv, ActionSpace
from sympy.geometry import Point


class RandomAgent:
    """The world's simplest agent!"""

    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, observation, reward, done):
        return self.action_space.sample()


class AlmostCleverAgent:
    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, observation, reward, done):
        pos, goal = observation
        possible_actions = [self.action_space.sample() for _ in range(5)]
        best_action, best_distance = None, 10000000
        for action in possible_actions:
            angle = math.radians(action)
            new_pos = pos + Point(math.cos(angle), math.sin(angle)) * 1
            distance = goal.distance(new_pos)
            if distance < best_distance:
                best_distance, best_action = distance, action
        return best_action


class NNAgent:
    def __init__(self, agent, nn):
        self.agent = agent
        self.nn = nn

    def act(self, observation, reward, done):
        if random.random() < epsilon:
            action = random.choice(action_space)
        else:
            qval = self.nn.predict(np.array([observation]))
            action = np.argmax(qval)
        return action


if __name__ == "__main__":
    action_space = ActionSpace(90)
    env = OmniEnv(size=30, action_space=action_space)
    agent = AlmostCleverAgent(action_space)

    ob, reward, done = env.reset(), 0, False
    for step in range(20):
        action = agent.act(ob, reward, done)
        new_ob, reward, done, _ = env.step(action)

        # Fit model

        ob = new_ob
        if done:
            break
        env.render()

    # Close the env and write monitor result info to disk
    env.close()
