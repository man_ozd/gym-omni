import math

from envs.omni_env import OmniEnv, ActionSpace
from shapely.geometry import Point


class RandomAgent:
    """The world's simplest agent!"""

    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, observation, reward, done):
        return self.action_space.sample()


class AlmostCleverAgent:
    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, observation, reward, done):
        print(observation)
        pos, goal = observation
        possible_actions = [self.action_space.sample() for _ in range(5)]
        best_action, best_distance = None, 10000000
        for action in possible_actions:
            angle = math.radians(action)
            new_pos = Point(pos.x + math.cos(angle), pos.y + math.sin(angle))
            distance = goal.distance(new_pos)
            if distance < best_distance:
                best_distance, best_action = distance, action
        return best_action


class NNAgent:
    def __init__(self, action_space, nn):
        self.action_space
        self.nn = nn

    def act(self, observation, reward, done):
        action = self.predict(observation)


if __name__ == "__main__":
    action_space = ActionSpace(90)
    env = OmniEnv(
        (0, 0), delta=0.1, world_size=10, speed=0.5, agent_size=0.28, action_space=action_space
    )
    agent = AlmostCleverAgent(action_space)

    ob, reward, done = env.reset(), 0, False
    for step in range(20):
        action = agent.act(ob, reward, done)
        ob, reward, done, _ = env.step(action)
        if done:
            break
        env.render()

    # Close the env and write monitor result info to disk
    env.close()
