import math
import logging
import random

import gym
import numpy as np
from shapely.geometry import Point, MultiPoint, LineString, LinearRing

logger = logging.getLogger(__name__)


class ActionSpace:
    def __init__(self, angle_size):
        self.angles = range(0, 360, angle_size)
        self.actions = range(len(self.angles))

    def sample(self):
        return random.choice(self.actions)

    @property
    def size(self):
        return len(self.actions)


class OmniEnv(gym.Env):
    metadata = {"render.modes": ["human"]}

    def __init__(
        self,
        start_xy,
        delta,
        world_size,
        agent_size: int,
        speed,
        action_space: ActionSpace,
        view_angle=1,
        view_distance=3.6,
        n_obstacles=0,
    ):
        self.start_xy = start_xy
        self.world_size = world_size
        self.agent_size = agent_size
        self.speed = speed
        self.view_angles = range(0, 360, view_angle)
        self.delta = delta
        self.n_obstacles = n_obstacles
        self.view_distance = view_distance
        
        angle = 360 // action_space.size
        self.move_angles = [angle*i for i in range(action_space.size)]
        self.goal, self.agent, self.epoch = None, None, None
        self.reset()

    @property
    def observation_space_n(self):
        return len(self.view_angles)

    def reset(self):
        goal_x, goal_y = (
            random.choice([x for x in range(1, self.world_size // 2 - 1)]),
            random.choice([y for y in range(1, self.world_size // 2 - 1)]),
        )
        self.goal = Point(goal_x, goal_y)

        self.agent = Point(0.0, 0.0)
        self.distance = self.agent.distance(self.goal)
        self.obstacles = self._generate_obstacles()
        self.epoch = 0
        observation = np.concatenate(
            (np.array([*self.agent.xy, *self.goal.xy]), self._ranges()), axis=None
        )
        return observation

    def step(self, action):
        angle = self.move_angles[action]
        rads = math.radians(angle)
        self.agent = Point(
            self.agent.x + math.cos(rads) * self.speed,
            self.agent.y + math.sin(rads) * self.speed,
        )
        done = (
            self.agent.x < -self.world_size // 2
            or self.agent.x > self.world_size // 2
            or self.agent.y < -self.world_size // 2
            or self.agent.y > self.world_size // 2
        )
        ranges = self._ranges()
        observation = np.concatenate(
            (np.array([*self.agent.xy, *self.goal.xy]), ranges), axis=None
        )
        if not all(range_ > self.agent_size for range_ in ranges):
            return observation, -2, True, None
        if self.agent.distance(self.goal) < self.delta:
            return observation, 1, done, None
        return observation, -self.agent.distance(self.goal), done, None

    def _generate_obstacles(self):
        x, y = self.world_size / 2, self.world_size / 2
        ring = LinearRing([(x, y), (-x, y), (-x, -y), (x, -y)])
        for i in range(self.n_obstacles):
            polygon_type = random.randint(0, 0)
            if polygon_type == 0:
                x, y = (
                    random.randint(0, self.world_size - 1),
                    random.randint(0, self.world_size - 1),
                )
        self.obstacles = [ring]
        return self.obstacles

    def _ranges(self):
        ranges = []
        for angle in self.view_angles:
            for obstacle in self.obstacles:
                rads = math.radians(angle)
                max_x, max_y = (
                    self.agent.x + self.view_distance * math.cos(rads),
                    self.agent.y + self.view_distance * math.sin(rads),
                )
                line = LineString([self.agent, (max_x, max_y)])
                if not line.intersects(obstacle):
                    ranges.append(self.view_distance)
                    break
                if self.agent.intersects(obstacle):
                    ranges.append(0.0)
                    break

                distance = LineString([self.agent, obstacle.intersection(line)]).length
                ranges.append(distance)
        return ranges
    
    def render(self):
        field = [[" " for _ in range(self.world_size)] for _ in range(self.world_size)]
        field[int(self.agent.x)][int(self.agent.y)] = "A"
        field[int(self.goal.x)][int(self.goal.y)] = "G"
        print(f"Epoch: {self.epoch}")
        print("-" * (self.world_size + 2))
        for row in field:
            print("|" + "".join(row) + "|")
        print("-" * (self.world_size + 2))
