from gym.envs.registration import register

register(
    id="omni-v0", entry_point="gym_omni.envs:OmniEnv"
)
