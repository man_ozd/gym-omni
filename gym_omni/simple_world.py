import numpy as np
import sympy
import random
from math import radians, cos, sin, sqrt
from sympy import intersection, Point
from sympy.plotting import plot
from sympy.geometry import Segment, Circle, Polygon


class World:
    def __init__(self, size: tuple, n_obstacles=0, obstacle_size=0):
        x, y = size
        self.x = x / 2
        self.y = y / 2
        self.n_obstacles = n_obstacles
        self.obstacle_size = obstacle_size
        self.polygon = Polygon(
            Point(self.x, self.y),
            Point(self.x, -self.y),
            Point(-self.x, -self.y),
            Point(-self.x, self.y),
        )
        self.obstacles = [self.polygon]

    def generate_obstacles(self, goal_xy):
        for i in range(self.n_obstacles):
            center = (
                round(
                    random.uniform(
                        -self.x + self.obstacle_size, self.x - self.obstacle_size
                    ),
                    1,
                ),
                round(
                    random.uniform(
                        -self.y + self.obstacle_size, self.y - self.obstacle_size
                    ),
                    1,
                ),
            )
            self.obstacles.append(Circle(center, self.obstacle_size))

    def get_dist_to_obst(self, position, direction, max_distance):
        x, y = position
        rads = radians(direction)
        ray_limit = (x + max_distance * cos(rads), y + max_distance * sin(rads))
        laser_ray = Segment(position, ray_limit)
        min_intersection_distance = max_distance

        # for obstacle in self.obstacles:
        for point in intersection(self.polygon, laser_ray):
            if type(point) == sympy.geometry.line.Segment2D:
                return 0
            x, y = float(point.x) - x, float(point.y) - y
            distance = sqrt(x * x + y * y)
            if distance < min_intersection_distance:
                min_intersection_distance = distance
        if min_intersection_distance != max_distance:
            min_intersection_distance = min_intersection_distance + random.uniform(
                -max_distance * 0.01, max_distance * 0.01
            )
        return min_intersection_distance

    def get_random_position(self, x_indent, y_indent):
        return (
            round(random.uniform(-self.x + x_indent, self.x - x_indent), 1),
            round(random.uniform(-self.y + y_indent, self.y - y_indent), 1),
        )


class Agent:
    def __init__(
        self,
        world: World,
        start_xy: tuple,
        goal_xy: tuple,
        size=0.28,
        speed=0.5,
        move_angles=range(0, 360, 5),
        view_angles=range(0, 360, 30),
        view_distance=3.6,
    ):
        self.start_xy = start_xy
        self.goal_xy = goal_xy
        self.size = size
        self.speed = speed
        self.world = world
        self.view_distance = view_distance
        self.move_angles = move_angles
        self.view_angles = view_angles
        self.agent = Circle(start_xy, size)
        self.laser_distances = [
            self.world.get_dist_to_obst(start_xy, angle, max_distance=view_distance)
            for angle in self.view_angles
        ]

    def move(self, direction):
        rads = radians(direction)
        x, y = self.agent.center.x, self.agent.center.y
        new_x, new_y = (
            round(x + self.speed * cos(rads), 2),
            round(y + self.speed * sin(rads), 2),
        )
        self.agent = Circle((new_x, new_y), self.size)
        self.laser_distances = [
            self.world.get_dist_to_obst(
                (new_x, new_y), angle, max_distance=self.view_distance
            )
            for angle in self.view_angles
        ]

    def update_speed(self, speed):
        self.speed = speed

    def laser_data(self):
        return np.array(self.laser_distances)

    def gxy(self):
        return np.array([self.goal_xy[0], self.goal_xy[1]])

    def xy(self):
        return np.array([float(self.agent.center.x), float(self.agent.center.y)])

    def reset(self):
        self.start_ = self.world.get_random_position(self.size, self.size)
        self.goal_xy = self.world.get_random_position(self.size, self.size)
        self.agent = Circle(self.start_xy, self.size)
        self.laser_distances = [
            self.world.get_dist_to_obst(
                self.start_xy, angle, max_distance=self.view_distance
            )
            for angle in self.view_angles
        ]
